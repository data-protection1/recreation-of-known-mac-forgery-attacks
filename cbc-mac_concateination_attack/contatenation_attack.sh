printf "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00" > head.dat
message1='Wat dslfsadfpfdsa'
message2='Oops, Sorry, I just remember that I have a meeting very soon in the morning.'
key=$(openssl rand -hex 16)

echo $key      > key.dat
echo $message1 > mess1.dat
echo $message2 > mess2.dat


cat head.dat mess1.dat > tmp1.dat
cat head.dat mess2.dat > tmp2.dat

# encrit with cbc-mac the messages and get just the taf
openssl enc -aes-128-cbc -K $key -iv 0 -in tmp1.dat | tail -c 16 > tag1.dat
openssl enc -aes-128-cbc -K $key -iv 0 -in tmp2.dat | tail -c 16 > tag2.dat



# get the padding of the message 1
openssl enc -aes-128-cbc -K $key -iv 0 -in mess1.dat -out cipher.dat
openssl enc -d -aes-128-cbc -K $key -iv 0 -nopad -in cipher.dat -out padded.dat


cat head.dat padded.dat tag1.dat mess2.dat > forgery.dat

openssl enc -aes-128-cbc -K $key -iv 0 -in forgery.dat -out forgery.out


echo "------- tag 2 -------"
cat tag2.dat | xxd
echo "------- forgery -----"
cat forgery.out | tail -c 16 > forgery.tag
cat forgery.tag | xxd

rm tmp1.dat
rm tmp2.dat
rm head.dat
